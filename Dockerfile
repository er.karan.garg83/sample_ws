# Use a lightweight base image, such as NGINX
FROM nginx:latest

# Copy the website files to the container
COPY . /usr/share/nginx/html

# Expose port 80 for web traffic
EXPOSE 80

# Command to start the web server
CMD ["nginx", "-g", "daemon off;"]